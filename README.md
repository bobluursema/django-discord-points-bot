# A Discord bot to give and bet points

[View the documentation](https://bobluursema.gitlab.io/django-discord-points-bot).

## Create link to enable bot for server

Go to the Discord developer portal, open the application, go to OAuth2 -> URL Generator.

Select scopes: `bot` and `applications.commands` and bot permissions `Send Messages`.

## Setup dev env

```sh
python3 -m venv .venv
# Activate env and then
pip install -r requirements.txt
```

## TODO

- Create some test-commands that enable you to let the application do some actions like betting or creating games so that you can test that by yourself
- Add bot to project-sunset (create new application), setup recurring task for anniversary, setup deploy task for discord commands, and then replace the link in the documentation of this project to that deployment.
- Add support for multiple bets at the same time
  - How to implement resolve/close/view commands if there are multiple bets?