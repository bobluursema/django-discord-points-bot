from unittest.mock import patch

from discordbot.models import Guild
from django.test import Client, TestCase

from tests.utils import command_data

c = Client(HTTP_X_SIGNATURE_ED25519="foo", HTTP_X_SIGNATURE_TIMESTAMP="bar")


@patch("discordbot.views.verify_key", new=lambda *args: True)
class PointsManagementTests(TestCase):
    fixtures = ["test_data.json"]

    def test_setting_anniversary(self):
        response = c.post('/interactions', {
            "type": 2,
            "guild_id": "1",
            "data": command_data('points-manage', 'set-anniversary', {'amount': 50, 'channel': '3'})
        }, content_type="application/json")
        self.assertEqual(200, response.status_code)
        guild = Guild.objects.get(guild_id="1")
        self.assertEqual(guild.anniversary_points, 50)
        self.assertEqual(guild.anniversary_channel, "3")

    def test_set_infinite_role(self):
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': "1",
            'data': command_data('points-manage', 'set-infinite-role', {'role': '25'})
        }, content_type="application/json")
        self.assertEqual(200, response.status_code)
        guild = Guild.objects.get(guild_id="1")
        self.assertEqual(guild.infinite_points_role, "25")

    def test_set_points_name(self):
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': "1",
            'data': command_data('points-manage', 'set-points-name', {'prefix': 'Bonus'})
        }, content_type="application/json")
        self.assertEqual(200, response.status_code)
        guild = Guild.objects.get(guild_id="1")
        self.assertEqual(guild.points_prefix, "Bonus")
