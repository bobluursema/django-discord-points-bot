

def command_data(name, subname=None, values: dict = None):
    data = {
        'name': name,
    }
    if subname is not None:
        data['options'] = [{
            'name': subname
        }]
    if values is not None:
        options = [{'name': key, 'value': value} for key, value in values.items()]
        if subname is not None:
            data['options'][0]['options'] = options
        else:
            data['options'] = options
    return data


def caller_data(joined_at="2022-10-10T12:12:12.383000+00:00", roles=["9"], nick=None, user__username="caller", user__id="10"):
    return {
        "joined_at": joined_at,
        'roles': roles,
        'nick': nick,
        'user': {
            'username': user__username,
            'id': str(user__id)
        }
    }
