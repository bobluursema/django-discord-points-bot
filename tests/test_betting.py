from unittest.mock import patch
from django.test import TestCase
from django.test.client import Client
from discordbot.models import Bet, BettingGame, GuildMember, Guild

from tests.utils import caller_data, command_data

c = Client(HTTP_X_SIGNATURE_ED25519="foo", HTTP_X_SIGNATURE_TIMESTAMP="bar")


@patch("discordbot.views.verify_key", new=lambda *args: True)
class BettingTest(TestCase):
    fixtures = ["test_data.json"]

    def test_betting_game(self):
        # Create game
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'create-game', {'bet': 'Yay or Nay?'}),
            'member': caller_data(user__id=10),
            'token': 'noop-token'
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, BettingGame.objects.count(), msg=response.json()['data']['content'])

        # Other user places bet via slash command but with too many points
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'place-bet', {'yes_or_no': 'yes', 'amount': 20}),
            'member': caller_data(user__id=11)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(0, Bet.objects.count(), msg=response.json()['data']['content'])

        # User 11 places bet via slash command
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'place-bet', {'yes_or_no': 'yes', 'amount': 5}),
            'member': caller_data(user__id=11)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, Bet.objects.count(), msg=response.json()['data']['content'])

        # User 12 presses button to place bet
        response = c.post('/interactions', {
            'type': 3,
            'guild_id': '10',
            'data': {
                'custom_id': 'no'
            },
            'message': {
                'interaction': {
                    'name': 'betting'
                }
            },
            'member': caller_data(user__id=12)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)

        # User 12 places bet via modal
        response = c.post('/interactions', {
            'type': 5,
            'guild_id': '10',
            'data': {
                'custom_id': 'place_bet',
                'components': [{
                    'components': [{
                        'custom_id': 'amount_on_no',
                        'value': 4
                    }]
                }]
            },
            'message': {
                'interaction': {
                    'name': 'betting'
                }
            },
            'member': caller_data(user__id=12)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(2, Bet.objects.count(), msg=response.json()['data']['content'])

        # Initiator tries to place bet
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'place-bet', {'yes_or_no': 'yes', 'amount': 5}),
            'member': caller_data(user__id=10)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(2, Bet.objects.count(), msg=response.json()['data']['content'])

        # View bets
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'view-bets'),
            'member': caller_data(user__id=10)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)

        # User 11 tries to resolve
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'resolve-bet'),
            'member': caller_data(user__id=11)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, BettingGame.objects.filter(resolved=False).count())

        # Initiator resolves bet
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'resolve-bet', {'result': 'yes'}),
            'member': caller_data(user__id=10)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, BettingGame.objects.filter(resolved=True).count())

        # Points have been distributed
        self.assertEqual(20, GuildMember.objects.get(discord_id='11').points)
        self.assertEqual(9, GuildMember.objects.get(discord_id='12').points)

    def test_betting_game_2(self):
        """
        Check if a non-initiator can't close the bet and if you can't place a bet after closing.
        """
        # Create game
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'create-game', {'bet': 'Yay or Nay?'}),
            'member': caller_data(user__id=10),
            'token': 'noop-token'
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, BettingGame.objects.count(), msg=response.json()['data']['content'])

        # Other user tries to close
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'close-betting'),
            'member': caller_data(user__id=11)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, BettingGame.objects.filter(closed=False).count())

        # Initiator closes
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'close-betting'),
            'member': caller_data(user__id=10)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, BettingGame.objects.filter(closed=True).count())

        # Other user can't place a bet
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'place-bet', {'yes_or_no': 'yes', 'amount': 5}),
            'member': caller_data(user__id=11)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(0, Bet.objects.count(), msg=response.json()['data']['content'])

    def test_betting_game_3(self):
        """
        Check if the distribution logic is correct
        """
        guild = Guild.objects.get(pk=10)
        initiator = GuildMember.objects.get(pk=10)
        user_11 = GuildMember.objects.get(pk=11)
        user_12 = GuildMember.objects.get(pk=12)
        user_13 = GuildMember.objects.get(pk=13)
        game = BettingGame(guild=guild, initiator=initiator, question="", original_message_token="noop-token")
        game.save()
        Bet(game=game, better=user_11, amount=5, bet_on="YES").save()
        Bet(game=game, better=user_12, amount=5, bet_on="YES").save()
        Bet(game=game, better=user_13, amount=8, bet_on="NO").save()

        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'resolve-bet', {'result': 'yes'}),
            'member': caller_data(user__id=10)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        user_11.refresh_from_db()
        user_12.refresh_from_db()
        user_13.refresh_from_db()
        self.assertEqual(20, user_11.points)
        self.assertEqual(17, user_12.points)
        self.assertEqual(12, user_13.points)

    def test_betting_game_4(self):
        """
        Check if the distribution logic is correct
        """
        guild = Guild.objects.get(pk=10)
        initiator = GuildMember.objects.get(pk=10)
        user_11 = GuildMember.objects.get(pk=11)
        user_12 = GuildMember.objects.get(pk=12)
        user_13 = GuildMember.objects.get(pk=13)
        game = BettingGame(guild=guild, initiator=initiator, question="", original_message_token="noop-token")
        game.save()
        Bet(game=game, better=user_11, amount=10, bet_on="YES").save()
        Bet(game=game, better=user_12, amount=5, bet_on="YES").save()
        Bet(game=game, better=user_13, amount=9, bet_on="NO").save()

        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'resolve-bet', {'result': 'yes'}),
            'member': caller_data(user__id=10)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        user_11.refresh_from_db()
        user_12.refresh_from_db()
        user_13.refresh_from_db()
        self.assertEqual(22, user_11.points)
        self.assertEqual(16, user_12.points)
        self.assertEqual(11, user_13.points)

    def test_betting_game_5(self):
        """
        Check if the distribution logic is correct
        """
        guild = Guild.objects.get(pk=10)
        initiator = GuildMember.objects.get(pk=10)
        user_11 = GuildMember.objects.get(pk=11)
        user_12 = GuildMember.objects.get(pk=12)
        user_13 = GuildMember.objects.get(pk=13)
        game = BettingGame(guild=guild, initiator=initiator, question="", original_message_token="noop-token")
        game.save()
        Bet(game=game, better=user_11, amount=10, bet_on="YES").save()
        Bet(game=game, better=user_12, amount=5, bet_on="YES").save()
        Bet(game=game, better=user_13, amount=8, bet_on="NO").save()

        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('betting', 'resolve-bet', {'result': 'yes'}),
            'member': caller_data(user__id=10)
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        user_11.refresh_from_db()
        user_12.refresh_from_db()
        user_13.refresh_from_db()
        self.assertEqual(21, user_11.points)
        self.assertEqual(16, user_12.points)
        self.assertEqual(12, user_13.points)
