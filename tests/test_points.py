from copy import deepcopy
from unittest.mock import patch

from discordbot.models import GuildMember, PointsTransaction
from django.test import Client, TestCase

from tests.utils import caller_data, command_data

c = Client(HTTP_X_SIGNATURE_ED25519="foo", HTTP_X_SIGNATURE_TIMESTAMP="bar")


@patch("discordbot.views.verify_key", new=lambda *args: True)
class PointsTests(TestCase):
    fixtures = ["test_data.json"]

    def test_give_points_and_create_victim(self):
        request = {
            'type': 2,
            'guild_id': '10',
            'data': command_data('points', 'give', {'user': '5', 'amount': 3, 'reason': 'for testing'}),
            'member': caller_data()
        }
        request['data']['resolved'] = {
            'users': {
                "5": {
                    'username': "victim",
                }
            },
            'members': {
                '5': {
                    'joined_at': "2021-10-10T12:13:14.987000+00:00",
                    "nick": None
                }
            }
        }
        response = c.post('/interactions', request, content_type="application/json")
        self.assertEqual(200, response.status_code)
        caller = GuildMember.objects.get(discord_id="10")
        self.assertEqual(7, caller.points)
        victim = GuildMember.objects.get(discord_id="5")
        self.assertEqual(3, victim.points)
        transaction = PointsTransaction.objects.first()
        self.assertEqual(caller, transaction.from_member)
        self.assertEqual(victim, transaction.to_member)
        self.assertEqual(3, transaction.amount)

    def test_give_points_infinite(self):
        request = {
            'type': 2,
            'guild_id': '10',
            'data': command_data('points', 'give', {'user': '5', 'amount': 3, 'reason': 'for testing'}),
            'member': caller_data(roles=['10'])
        }
        request['data']['resolved'] = {
            'users': {
                "5": {
                    'username': "victim",
                }
            },
            'members': {
                '5': {
                    'joined_at': "2021-10-10T12:13:14.987000+00:00",
                    "nick": None
                }
            }
        }
        response = c.post('/interactions', request, content_type="application/json")
        self.assertEqual(200, response.status_code)
        caller = GuildMember.objects.get(discord_id="10")
        self.assertEqual(10, caller.points)
        victim = GuildMember.objects.get(discord_id="5")
        self.assertEqual(3, victim.points)

    def test_leaderboard(self):
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('points', 'leaderboard')
        }, content_type="application/json")
        self.assertEqual(200, response.status_code)

    def test_check_score(self):
        response = c.post('/interactions', {
            'type': 2,
            'guild_id': '10',
            'data': command_data('points', 'check-score', {'user': '10'})
        }, content_type="application/json")
        self.assertEqual(200, response.status_code)
