#!/bin/bash

set -e

docker build -t discord-bot:latest .

set +e
docker stop discord
docker rm discord

docker run \
  --publish 80:80 \
  --name discord \
  --net mynet \
  --mount type=bind,source=/home/bob/bobs-discord-bot,target=/etc/runserver \
  discord-bot
