from unittest.mock import patch
from django.test import TestCase
from django.test.client import Client

c = Client(HTTP_X_SIGNATURE_ED25519="foo", HTTP_X_SIGNATURE_TIMESTAMP="bar")


@patch("discordbot.views.verify_key", new=lambda *args: True)
class PingTest(TestCase):
    def test_ping(self):
        response = c.post('/interactions', {
            'type': 1
        }, content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, response.json()['type'])
