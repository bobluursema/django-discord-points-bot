def get_name(data, user_id):
    if user_id == data['member']['user']['id']:
        if data['member']['nick'] is not None:
            return data['member']['nick']
        else:
            return data['member']['user']['username']
    else:
        member = data['data']['resolved']['members'][user_id]
        if member['nick'] is not None:
            return member['nick']
        else:
            return data['data']['resolved']['users'][user_id]['username']


def get_current_points_string(data, user_id, guild_member):
    return f'{get_name(data, user_id)} has {guild_member.guild.points(guild_member.points)}'


def get_option_value(data, option_name):
    for option in data['data']['options']:
        if option['name'] == option_name:
            return option['value']
    for option in data['data']['options'][0]['options']:
        if option['name'] == option_name:
            return option['value']
    return None
