from django.urls import path

from .views import interactions, last_call

urlpatterns = [
    path('interactions', interactions),
    path('last-call', last_call)
]
