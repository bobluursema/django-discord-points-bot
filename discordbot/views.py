import json

from discord_interactions import (InteractionResponseType, InteractionType,
                                  verify_key)
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from discordbot.discord.api import Discord
from discordbot.interactions.betting import bet_modal, betting, place_bet
from discordbot.interactions.points import points
from discordbot.interactions.points_management import manage_points


@user_passes_test(lambda u: u.is_superuser)
def last_call(request):
    indented_call = json.dumps(json.loads(last_bot_call), indent=2)
    return HttpResponse(f'<div style="white-space: pre">{indented_call}</div>')


last_bot_call = ""


@csrf_exempt
def interactions(request):
    global last_bot_call
    last_bot_call = request.body
    # Validate call
    signature = request.headers.get('X-Signature-Ed25519')
    timestamp = request.headers.get('X-Signature-Timestamp')
    if signature is None or timestamp is None or not verify_key(request.body, signature, timestamp, settings.DISCORD_BOT['CLIENT_PUBLIC_KEY']):
        return HttpResponse('Bad request signature', status=401)
    # Respond to Pings
    data = json.loads(request.body)
    if data['type'] == InteractionType.PING:
        return JsonResponse({'type': InteractionResponseType.PONG})
    # Process request
    elif data['type'] == InteractionType.APPLICATION_COMMAND:
        if data['data']['name'] == 'points':
            return points(data)
        elif data['data']['name'] == 'points-manage':
            return manage_points(data)
        elif data['data']['name'] == 'betting':
            return betting(data)
    elif data['type'] == InteractionType.MESSAGE_COMPONENT:
        if data["message"]["interaction"]["name"] == "betting":
            yes_or_no = data['data']['custom_id']
            if yes_or_no in ['yes', 'no']:
                return bet_modal(
                    data["guild_id"],
                    yes_or_no,
                    data["member"]['user']['id'])
    elif data['type'] == InteractionType.MODAL_SUBMIT:
        if data['data']['custom_id'] == 'place_bet':
            return place_bet(
                data["guild_id"],
                data["data"]["components"][0]["components"][0]["custom_id"][10:],
                data["data"]["components"][0]["components"][0]["value"],
                data["member"]['user']['id'])
    return JsonResponse(Discord.interaction_respond_message('Unknown action'))
