from django.contrib import admin

from .models import Bet, BettingGame, Guild, GuildMember, LastAnniversaryParsed, PointsTransaction

admin.site.register(Guild)
admin.site.register(GuildMember)
admin.site.register(PointsTransaction)
admin.site.register(BettingGame)
admin.site.register(Bet)
admin.site.register(LastAnniversaryParsed)
