from datetime import date

from django.db import models
from django.db.models import Sum

from discordbot.discord.api import Discord
from discordbot.discord.types import ButtonStyle, ComponentType


class LastAnniversaryParsed(models.Model):
    last_anniversary_check = models.DateField(default=date.today)


class Guild(models.Model):
    guild_id = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    infinite_points_role = models.CharField(max_length=100, null=True, blank=True)
    betting_game_role = models.CharField(max_length=100, null=True, blank=True)
    anniversary_points = models.IntegerField(null=True, blank=True)
    anniversary_channel = models.CharField(max_length=100, null=True, blank=True)
    points_prefix = models.CharField(max_length=10, null=True, blank=True)

    def __str__(self):
        return self.name

    def points(self, number=None, plural=True):
        number_string = "" if number is None else f'{number} '
        if number is not None and number in [-1, 1]:
            plural = False
        if self.points_prefix is not None:
            return f'{number_string}{self.points_prefix}Point{"s" if plural else ""}'
        else:
            return f'{number_string}point{"s" if plural else ""}'


class GuildMember(models.Model):
    discord_id = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    guild = models.ForeignKey(Guild, on_delete=models.CASCADE)
    points = models.IntegerField()
    join_date = models.DateField()

    class Meta:
        unique_together = ['discord_id', 'guild']

    def __str__(self):
        return self.name

    def mention(self):
        return f"<@!{self.discord_id}>"

    def points_received(self):
        points = PointsTransaction.objects.filter(to_member=self, amount__gt=0).aggregate(Sum('amount'))['amount__sum']
        if points is None:
            return 0
        else:
            return points

    def points_lost(self):
        points = PointsTransaction.objects.filter(to_member=self, amount__lt=0).aggregate(Sum('amount'))['amount__sum']
        if points is None:
            return 0
        else:
            return abs(points)

    def points_given(self):
        points = PointsTransaction.objects.filter(from_member=self, amount__gt=0).aggregate(Sum('amount'))['amount__sum']
        if points is None:
            return 0
        else:
            return points

    def points_taken(self):
        points = PointsTransaction.objects.filter(from_member=self, amount__lt=0).aggregate(Sum('amount'))['amount__sum']
        if points is None:
            return 0
        else:
            return abs(points)

    def points_in_bet(self):
        points = Bet.objects.filter(better=self, game__resolved=False).aggregate(Sum('amount'))['amount__sum']
        if points is None:
            return 0
        else:
            return points

    def points_available(self):
        return self.points - self.points_in_bet()


class PointsTransaction(models.Model):
    from_member = models.ForeignKey(GuildMember, on_delete=models.CASCADE, related_name='outgoing_transactions')
    to_member = models.ForeignKey(GuildMember, on_delete=models.CASCADE, related_name='incoming_transactions')
    amount = models.IntegerField()


YES_NO = [
    ('YES', 'Yes'),
    ('NO', 'No')
]


class BettingGame(models.Model):
    guild = models.ForeignKey(Guild, on_delete=models.CASCADE)
    initiator = models.ForeignKey(GuildMember, on_delete=models.SET_NULL, null=True)
    question = models.TextField(max_length=200)
    closed = models.BooleanField(default=False)
    resolved = models.BooleanField(default=False)
    result = models.CharField(max_length=3, choices=YES_NO, null=True, blank=True)
    created_time = models.DateTimeField(auto_now_add=True)
    original_message_token = models.TextField()

    def __str__(self):
        return self.question

    def betting_message(self):
        return {
            'content': self.question,
            'components': [{
                'type': ComponentType.ACTION_ROW,
                'components': [{
                    'type': ComponentType.BUTTON,
                    'style': ButtonStyle.SUCCESS,
                    'label': 'Bet on Yes!',
                    'custom_id': 'yes'
                }, {
                    'type': ComponentType.BUTTON,
                    'style': ButtonStyle.DANGER,
                    'label': 'Bet on No!',
                    'custom_id': 'no'
                }]
            }]
        }

    def disable_betting_buttons(self):
        if self.original_message_token is None or self.original_message_token == 'noop-token':
            return
        d = Discord()
        message = self.betting_message()
        message['components'][0]['components'][0]['disabled'] = True
        message['components'][0]['components'][1]['disabled'] = True
        d.edit_original_interaction_response(self.original_message_token, message)


class Bet(models.Model):
    game = models.ForeignKey(BettingGame, on_delete=models.CASCADE, related_name='bets')
    better = models.ForeignKey(GuildMember, on_delete=models.CASCADE)
    amount = models.PositiveIntegerField()
    bet_on = models.CharField(max_length=3, choices=YES_NO)

    class Meta:
        unique_together = ['game', 'better']

    def __str__(self):
        return f'{self.game} - {self.better}'
