from datetime import date

from discord_interactions import InteractionResponseType
from discordbot.discord.api import Discord
from discordbot.discord.types import ComponentType, TextInputStyle
from discordbot.models import Bet, BettingGame, Guild, GuildMember
from discordbot.utils import get_option_value
from django.db import transaction
from django.db.models import Sum
from django.http import JsonResponse


def betting(data):
    sub_command = data['data']['options'][0]['name']
    if sub_command == 'create-game':
        return create_game(data)
    elif sub_command == 'place-bet':
        return place_bet(
            data['guild_id'],
            get_option_value(data, 'yes_or_no'),
            get_option_value(data, "amount"),
            data['member']['user']['id'])
    elif sub_command == 'resolve-bet':
        return resolve_bet(data)
    elif sub_command == 'view-bets':
        return view_bets(data)
    elif sub_command == 'close-betting':
        return close_betting(data)


def close_betting(data):
    guild = Guild.objects.get(guild_id=data['guild_id'])
    caller = GuildMember.objects.get(guild=guild, discord_id=data['member']['user']['id'])
    try:
        game = BettingGame.objects.get(guild=guild, resolved=False)
    except BettingGame.DoesNotExist:
        return JsonResponse(Discord.interaction_respond_message('There is no game going on at the moment'))
    if game.initiator != caller:
        return JsonResponse(Discord.interaction_respond_message("You didn't initiate the game, so you can't close betting"))
    game.closed = True
    game.save()
    try:
        game.disable_betting_buttons()
    except Exception as ex:
        print(ex)
    return JsonResponse(Discord.interaction_respond_message("The betting is closed! Now we wait for the result!"))


def view_bets(data):
    guild = Guild.objects.get(guild_id=data['guild_id'])
    try:
        game = BettingGame.objects.get(guild=guild, resolved=False)
    except BettingGame.DoesNotExist:
        return JsonResponse(Discord.interaction_respond_message('There is no game going on at the moment'))
    total_on_yes = 0
    total_on_no = 0
    bets = ""
    for bet in game.bets.all():
        bets += f"{bet.better.mention()} has betted {guild.points(bet.amount)} on {bet.bet_on.lower()}\n"
        if bet.bet_on == 'YES':
            total_on_yes += bet.amount
        else:
            total_on_no += bet.amount
    message = f"{game.question}\n\n{guild.points(total_on_yes)} on yes\n{guild.points(total_on_no)} on no\n\n{bets}"
    return JsonResponse(Discord.interaction_respond_message(message))


@transaction.atomic
def resolve_bet(data):
    guild = Guild.objects.get(guild_id=data['guild_id'])
    member = GuildMember.objects.get(guild=guild, discord_id=data['member']['user']['id'])
    game = BettingGame.objects.get(guild=guild, resolved=False)
    if member != game.initiator:
        return JsonResponse(Discord.interaction_respond_message(f"{member.mention()}, you can't resolve the game, {game.initiator.mention()} should do that.", False))
    result = get_option_value(data, 'result').upper()
    if result not in ['YES', 'NO']:
        return JsonResponse(Discord.interaction_respond_message(f'{member.mention()}, {get_option_value(data, "result")} should be "yes" or "no".'))
    game.result = result
    game.resolved = True
    game.save()
    message = f"{game.question}\n\nTurned out as {result.lower()}!\n\n"
    remaining_points_lost = game.bets.exclude(bet_on=result).aggregate(Sum('amount'))['amount__sum']
    remaining_points_won = game.bets.filter(bet_on=result).aggregate(Sum('amount'))['amount__sum']
    for bet in Bet.objects.filter(game=game):
        if bet.bet_on == result:
            earnings_share = round(remaining_points_lost * (bet.amount / remaining_points_won))
            bet.better.points += earnings_share
            message += f'{bet.better.mention()} won {guild.points(earnings_share)}!\n'
            remaining_points_won -= bet.amount
            remaining_points_lost -= earnings_share
        else:
            bet.better.points -= bet.amount
            message += f'{bet.better.mention()} lost {guild.points(bet.amount)}.\n'
        bet.better.save()
    try:
        game.disable_betting_buttons()
    except Exception as ex:
        print(ex)
    return JsonResponse(Discord.interaction_respond_message(message))


def create_game(data):
    guild, guild_created = Guild.objects.get_or_create(guild_id=data['guild_id'])
    caller, caller_created = GuildMember.objects.get_or_create(
        guild=guild,
        discord_id=data['member']['user']['id'],
        defaults={
            'name': data['member']['user']['username'],
            'join_date': date.fromisoformat(data['member']['joined_at'][:10]),
            'points': 0
        }
    )
    if guild.betting_game_role is not None and guild.betting_game_role not in data['member']['roles']:
        return JsonResponse(Discord.interaction_respond_message(f'Sorry, {caller.mention()} you are not allowed to start a betting game. You need the <@&{guild.betting_game_role}> role for that.', False))
    running_games = BettingGame.objects.filter(guild=guild, resolved=False).count()
    if running_games != 0:
        return JsonResponse(Discord.interaction_respond_message('There is already a betting game going on, that one should be resolved before starting a new one'))
    game = BettingGame(
        guild=guild,
        initiator=caller,
        question=get_option_value(data, 'bet'),
        original_message_token=data['token'])
    game.save()
    return JsonResponse({
        'type': InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
        'data': game.betting_message()
    })


def place_bet(guild_id, yes_or_no: str, amount: str, user_id):
    guild = Guild.objects.get(guild_id=guild_id)
    try:
        game = BettingGame.objects.get(guild=guild, resolved=False)
    except BettingGame.DoesNotExist:
        return JsonResponse(Discord.interaction_respond_message('There is no game going on at the moment.'))
    if game.closed:
        return JsonResponse(Discord.interaction_respond_message('The game is closed, you can no longer bet!'))
    member = GuildMember.objects.get(guild=guild, discord_id=user_id)
    existing_bet = list(Bet.objects.filter(game=game, better=member))
    if member == game.initiator:
        return JsonResponse(Discord.interaction_respond_message(f'{member.mention()}, you are not allowed to bet as you made the game.', False))
    if len(existing_bet) > 0:
        return JsonResponse(Discord.interaction_respond_message(f'{member.mention()}, you can only bet once in every game! You have already betted {existing_bet[0].amount} on {existing_bet[0].bet_on.lower()}.', False))
    y_n = yes_or_no.lower()
    if y_n.lower() not in ['yes', 'no']:
        return JsonResponse(Discord.interaction_respond_message(f'You typed {yes_or_no}, {member.mention()}. You should type "yes" or "no"', False))
    try:
        bet_amount = int(amount)
    except ValueError:
        return JsonResponse(Discord.interaction_respond_message(f'{amount} is not a valid number, {member.mention()}', False))
    if bet_amount <= 0:
        return JsonResponse(Discord.interaction_respond_message(f'You should bet at least 1, {member.mention()}', False))
    if member.points_available() < bet_amount:
        return JsonResponse(Discord.interaction_respond_message(f"{member.mention()}, you can't bet {guild.points(bet_amount)}. You only have {guild.points(member.points_available())}."))
    Bet(
        game=game,
        better=member,
        amount=bet_amount,
        bet_on=yes_or_no.upper()
    ).save()
    return JsonResponse(Discord.interaction_respond_message(f'{member.mention()} has betted {guild.points(bet_amount)} on {yes_or_no}!', False))


def bet_modal(guild_id, yes_or_no, user_id):
    guild = Guild.objects.get(guild_id=guild_id)
    member = GuildMember.objects.get(guild=guild, discord_id=user_id)
    return JsonResponse({
        "type": InteractionResponseType.MODAL,
        "data": {
            "title": 'Place bet',
            "custom_id": "place_bet",
            "components": [{
                "type": ComponentType.ACTION_ROW,
                "components": [{
                    "type": ComponentType.TEXT_INPUT,
                    "custom_id": f"amount_on_{yes_or_no.lower()}",
                    "style": TextInputStyle.SHORT,
                    "label": f'Bet on "{yes_or_no}"? ({guild.points(member.points_available())} available)'[:45]
                }]
            }]
        }
    })
