from datetime import date

from discordbot.discord.api import Discord
from discordbot.models import Guild, GuildMember, PointsTransaction
from discordbot.utils import (get_current_points_string, get_name,
                              get_option_value)
from django.db import transaction
from django.http import JsonResponse


def points(data):
    sub_command = data['data']['options'][0]['name']
    if sub_command == 'give':
        return give_or_take(data)
    elif sub_command == 'leaderboard':
        return leaderboard(data)
    elif sub_command == 'check-score':
        return check_score(data)
    else:
        return JsonResponse(Discord.interaction_respond_message('Unknown action'))


def check_score(data):
    guild, guild_created = Guild.objects.get_or_create(guild_id=data['guild_id'])
    victim_id = get_option_value(data, "user")
    try:
        victim = GuildMember.objects.get(
            guild=guild,
            discord_id=victim_id)
    except GuildMember.DoesNotExist:
        return JsonResponse(Discord.interaction_respond_message(f'<@{victim_id}> has never received {guild.points()}.'))
    guild_members = list(GuildMember.objects.filter(guild=guild).order_by('-points'))
    index_of_victim = guild_members.index(victim)
    message = f"{victim.mention()} has {guild.points(victim.points)}\n\n"
    if len(guild_members) > index_of_victim+1:
        behinder = guild_members[index_of_victim+1]
        message += f"{behinder.mention()} is behind them by {guild.points(victim.points - behinder.points)}\n"
    if index_of_victim != 0:
        aheader = guild_members[index_of_victim-1]
        message += f"{aheader.mention()} is ahead of them by {guild.points(aheader.points - victim.points)}\n\n"
    message += f'They have received {guild.points(victim.points_received())}\n'
    message += f'They have lost {guild.points(victim.points_lost())}\n'
    message += f'They have given {guild.points(victim.points_given())} to others\n'
    message += f'They have taken away {guild.points(victim.points_taken())} from others\n'
    return JsonResponse(Discord.interaction_respond_message(message, False))


def leaderboard(data):
    guild, guild_created = Guild.objects.get_or_create(guild_id=data['guild_id'])
    guild_members = GuildMember.objects.filter(guild=guild).order_by('-points')[:10]
    message = f"{guild.points()} Leaderboard\n\n"
    for i, member in enumerate(guild_members, 1):
        message += f"{i}) {member.mention()} - {guild.points(member.points)}\n"
    return JsonResponse(Discord.interaction_respond_message(message, False))


@transaction.atomic
def give_or_take(data):
    # Retrieve or create objects from the database
    guild, guild_created = Guild.objects.get_or_create(guild_id=data['guild_id'])
    caller_id = data['member']['user']['id']
    caller, caller_created = GuildMember.objects.get_or_create(
        guild=guild,
        discord_id=caller_id,
        defaults={
            'name': data['member']['user']['username'],
            'join_date': date.fromisoformat(data['member']['joined_at'][:10]),
            'points': 0
        }
    )
    victim_id = get_option_value(data, 'user')
    victim, victim_created = GuildMember.objects.get_or_create(
        guild=guild,
        discord_id=victim_id,
        defaults={
            'name': data['data']['resolved']['users'][victim_id]['username'],
            'join_date': date.fromisoformat(data['data']['resolved']['members'][victim_id]['joined_at'][:10]),
            'points': 0
        })
    # Verify that the update is allowed
    if caller == victim:
        return JsonResponse(Discord.interaction_respond_message(f"You can't adjust your own {guild.points()}!"))
    amount = get_option_value(data, 'amount')
    if amount == 0:
        return JsonResponse(Discord.interaction_respond_message(f"You can't do this with 0 {guild.points()}, that doesn't make sense!"))
    reason = get_option_value(data, 'reason')
    reason_text = f"{' for ' if reason is not None else ''}{reason if reason is not None else ''}"
    if guild.infinite_points_role not in data['member']['roles']:
        if caller.points_available() < abs(amount):
            points_in_bet = caller.points_in_bet()
            betting_points = '' if points_in_bet == 0 else f' ({points_in_bet} unavailable due to bet)'
            return JsonResponse(Discord.interaction_respond_message(f"{get_name(data, caller_id)} tried to {'add' if amount > 0 else 'subtract'} {guild.points(abs(amount))} {'to' if amount > 0 else 'from'} {victim.mention()}{reason_text}, but {get_name(data, caller_id)} doesn't have enough points, they only have {caller.points} available{betting_points}."))
    # Apply update
    PointsTransaction(from_member=caller, to_member=victim, amount=amount).save()
    victim.points += amount
    victim.save()
    if guild.infinite_points_role not in data['member']['roles']:
        caller.points -= abs(amount)
        caller.save()
    return JsonResponse(Discord.interaction_respond_message(f'{get_name(data, caller_id)} {"added" if amount > 0 else "subtracted"} {guild.points(abs(amount))} {"to" if amount > 0 else "from"} {victim.mention()}{reason_text}.\n\n{get_current_points_string(data, victim_id, victim)}.\n{get_current_points_string(data, caller_id, caller)}.'))
