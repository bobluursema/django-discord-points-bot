from discordbot.discord.api import Discord
from discordbot.models import BettingGame, Guild
from discordbot.utils import get_option_value
from django.http import JsonResponse


def manage_points(data):
    sub_command = data['data']['options'][0]['name']
    if sub_command == 'set-infinite-role':
        return set_infinite_role(data)
    elif sub_command == 'set-anniversary':
        return set_anniversary(data)
    elif sub_command == 'set-points-name':
        return set_points_name(data)
    elif sub_command == 'delete-current-betting':
        return delete_current_betting(data)
    elif sub_command == 'set-create-betting-game-role':
        return betting_game_role(data)
    elif sub_command == 'view-settings':
        return view_settings(data)


def view_settings(data):
    guild, guild_created = Guild.objects.get_or_create(guild_id=data['guild_id'])
    message = "Settings:\n\n"
    if guild.points_prefix is not None:
        message += f"Custom points name: {guild.points()}\n"
    if guild.infinite_points_role is not None:
        message += f"Infinite points role: <@&{guild.infinite_points_role}>\n"
    else:
        message += f"Infinite points role not set, no one has infinite points\n"
    if guild.betting_game_role is not None:
        message += f"Create betting game role: <@&{guild.betting_game_role}>\n"
    else:
        message += f"Create betting game role not set, everyone can create a betting game\n"
    if guild.anniversary_points is not None:
        message += f"On their join date anniversary everyone will get {guild.points(guild.anniversary_points)} and a message in <#{guild.anniversary_channel}>"
    return JsonResponse(Discord.interaction_respond_message(message, False))


def betting_game_role(data):
    guild, guild_created = Guild.objects.get_or_create(guild_id=data['guild_id'])
    role_id = get_option_value(data, 'role')
    previous_role = guild.betting_game_role
    guild.betting_game_role = role_id
    guild.save()
    if previous_role is None:
        return JsonResponse(Discord.interaction_respond_message(f'Only users with the <@&{role_id}> can now start betting games!'))
    else:
        return JsonResponse(Discord.interaction_respond_message(f'Sorry <@&{previous_role}>, you lost your privileges. only users with the <@&{role_id}> role can now start betting games!'))


def delete_current_betting(data):
    guild, guild_created = Guild.objects.get_or_create(guild_id=data['guild_id'])
    count, _ = BettingGame.objects.filter(resolved=False, guild=guild).delete()
    return JsonResponse(Discord.interaction_respond_message(f"{count} betting game{'s' if count > 1 else ''} deleted"))


def set_anniversary(data):
    guild, guild_created = Guild.objects.get_or_create(guild_id=data['guild_id'])
    guild.anniversary_points = get_option_value(data, 'amount')
    guild.anniversary_channel = get_option_value(data, 'channel')
    guild.save()
    return JsonResponse(Discord.interaction_respond_message(f"On their join date anniversary everyone will get {guild.points(guild.anniversary_points)} and a message in <#{guild.anniversary_channel}>!"))


def set_infinite_role(data):
    guild, guild_created = Guild.objects.get_or_create(guild_id=data['guild_id'])
    role_id = get_option_value(data, 'role')
    previous_role = guild.infinite_points_role
    guild.infinite_points_role = role_id
    guild.save()
    if previous_role is None:
        return JsonResponse(Discord.interaction_respond_message(f'Everyone with the <@&{role_id}> can now distribute infinite {guild.points()}!'))
    else:
        return JsonResponse(Discord.interaction_respond_message(f'Sorry <@&{previous_role}>, you lost your privileges. Everyone with the <@&{role_id}> role can now distribute infinite {guild.points()}!'))


def set_points_name(data):
    prefix = get_option_value(data, 'prefix')
    if len(prefix) > 10:
        return JsonResponse(Discord.interaction_respond_message(f"The maximum length of the prefix is 10, your input was {len(prefix)}."))
    guild, guild_created = Guild.objects.get_or_create(guild_id=data['guild_id'])
    previous_prefix = guild.points_prefix
    guild.points_prefix = prefix
    guild.save()
    return JsonResponse(Discord.interaction_respond_message(f"{'Points' if previous_prefix is None else previous_prefix + 'Points'} have been renamed to {guild.points()}!"))
