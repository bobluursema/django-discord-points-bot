from datetime import date
from discordbot.TraceableBaseCommand import TraceableBaseCommand

from discordbot.discord.api import Discord
from discordbot.models import Guild, GuildMember, LastAnniversaryParsed

# https://codegolf.stackexchange.com/questions/4707/outputting-ordinal-numbers-1st-2nd-3rd#answer-4712


def ordinal(n): return "%d%s" % (n, "tsnrhtdd"[(n//10 % 10 != 1)*(n % 10 < 4)*n % 10::4])


class Command(TraceableBaseCommand):
    help = "Check for anniversaries"

    def handle(self, *args, **options):
        self.log("Checking anniversaries...")
        discord = Discord()
        messages_send = 0
        if LastAnniversaryParsed.objects.count() == 0:
            obj = LastAnniversaryParsed()
            obj.save()
        obj = LastAnniversaryParsed.objects.all()[0]
        last_checked_at = obj.last_anniversary_check
        today = date.today()
        for guild in Guild.objects.filter(anniversary_points__isnull=False, anniversary_channel__isnull=False):
            message = ""
            for member in GuildMember.objects.filter(guild=guild):
                for year in range(last_checked_at.year, today.year+1):
                    if member.join_date.month == 2 and member.join_date.day == 29:
                        anniversary = date(year, 2, 28)
                    else:
                        anniversary = date(year, member.join_date.month, member.join_date.day)
                    if anniversary > last_checked_at and anniversary <= today and anniversary > member.join_date:
                        member.points += guild.anniversary_points
                        member.save()
                        message += f"Congratulations with your {ordinal(year - member.join_date.year)} anniversary {member.mention()}! You gain {member.guild.points(guild.anniversary_points)}\n"
            if message != "":
                error = discord.post_channel_message(message, guild.anniversary_channel)
                messages_send += 1
                if error is not None:
                    self.stderr.write(error)
        obj.last_anniversary_check = date.today()
        obj.save()
        self.log(f"Sent messages in {messages_send} channels")
