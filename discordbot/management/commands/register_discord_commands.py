from django.core.management.base import CommandParser
from discordbot.TraceableBaseCommand import TraceableBaseCommand
from discordbot.discord.api import Discord


class Command(TraceableBaseCommand):
    help = "Register the slash commands in Discord"

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument('--test', action='store_true', help="Setup commands only in test guild")

    def handle(self, *args, **options):
        discord = Discord()
        if options['test']:
            self.log("Setting up commands in test guild")
            discord.setup_commands_for_test_guild()
        else:
            self.log("Setting up global commands")
            discord.setup_global_commands()
