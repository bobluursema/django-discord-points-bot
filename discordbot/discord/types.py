from enum import Enum


class TextInputStyle:
    SHORT = 1
    PARAGRAPH = 2


class ComponentType:
    ACTION_ROW = 1
    BUTTON = 2
    SELECT_MENU = 3
    TEXT_INPUT = 4


class ButtonStyle:
    PRIMARY = 1
    SECONDARY = 2
    SUCCESS = 3
    DANGER = 4
    LINK = 5


class CommandOptions:
    SUB_COMMAND = 1
    SUB_COMMAND_GROUP = 2
    STRING = 3
    INTEGER = 4
    BOOLEAN = 5
    USER = 6
    CHANNEL = 7
    ROLE = 8
    MENTIONABLE = 9
    NUMBER = 10
    ATTACHMENT = 11


class Permission(Enum):
    CREATE_INSTANT_INVITE = 0x1
    KICK_MEMBERS = 0x2
    BAN_MEMBERS = 0x4
    ADMINISTRATOR = 0x8
    MANAGE_CHANNELS = 0x10
    MANAGE_GUILD = 0x20
    ADD_REACTIONS = 0x40
    VIEW_AUDIT_LOG = 0x80
    PRIORITY_SPEAKER = 0x100


def create_permission_number(permissions: list[Permission]):
    return str(sum([p.value for p in permissions]))


def user_has_permission(user_permissions, permission: Permission):
    return (user_permissions & permission) == permission
