import requests
from discord_interactions import InteractionResponseType
from discordbot.discord.bot_commands import BETTING_COMMANDS, POINTS_COMMAND, POINTS_MANAGEMENT
from django.conf import settings


class Discord:
    def __init__(self):
        self.token = settings.DISCORD_BOT['TOKEN']
        self.app_id = settings.DISCORD_BOT['APP_ID']
        if 'GUILD_ID' in settings.DISCORD_BOT:
            self.guild_id = settings.DISCORD_BOT['GUILD_ID']
        self.base_url = 'https://discord.com/api/v10'
        self.session = requests.session()
        self.session.headers = {'Authorization': f'Bot {self.token}'}

    def edit_original_interaction_response(self, token: str, new_message: dict):
        response = self.session.patch(f'{self.base_url}/webhooks/{self.app_id}/{token}/messages/@original', json=new_message)
        if not response.ok:
            raise Exception(response.text)

    def post_channel_message(self, message, channel):
        response = self.session.post(f'{self.base_url}/channels/{channel}/messages', json={
            'content': message
        })
        if not response.ok:
            return response.text
        else:
            return None

    def delete_commands(self, names_to_delete):
        response = self.session.get(f'{self.base_url}/applications/{self.app_id}/guilds/{self.guild_id}/commands')
        if not response.ok:
            raise Exception(response.text)
        for command in response.json():
            if command['name'] in names_to_delete:
                response = self.session.delete(f'{self.base_url}/applications/{self.app_id}/guilds/{self.guild_id}/commands/{command["id"]}')
                if not response.ok:
                    raise Exception(response.text)

    def setup_commands_for_test_guild(self):
        response = self.session.put(
            f'{self.base_url}/applications/{self.app_id}/guilds/{self.guild_id}/commands',
            json=[POINTS_COMMAND, POINTS_MANAGEMENT, BETTING_COMMANDS])
        if not response.ok:
            raise Exception(response.text)
        self.session.close()

    def setup_global_commands(self):
        response = self.session.put(
            f'{self.base_url}/applications/{self.app_id}/commands',
            json=[POINTS_COMMAND, POINTS_MANAGEMENT, BETTING_COMMANDS])
        if not response.ok:
            raise Exception(response.text)
        self.session.close()

    @staticmethod
    def interaction_respond_message(message: str, allow_mentions: bool = True):
        if allow_mentions:
            return {
                'type': InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
                'data': {
                    'content': message.strip()
                }
            }
        else:
            return {
                'type': InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
                'data': {
                    'content': message.strip(),
                    "allowed_mentions": {
                        "parse": []
                    }
                }
            }
