from discordbot.discord.types import (CommandOptions, Permission,
                                      create_permission_number)

POINTS_COMMAND = {
    'name': 'points',
    'description': 'get points!',
    'options': [
        {
            "name": "give",
            "description": "give (or take away) points",
            "type": CommandOptions.SUB_COMMAND,
            "options": [
                {
                    'name': 'user',
                    'description': 'the user to affect',
                    'type': CommandOptions.USER,
                    'required': True
                },
                {
                    'name': 'amount',
                    'description': 'how many points, can be negative to take points away',
                    'type': CommandOptions.INTEGER,
                    'required': True
                },
                {
                    'name': 'reason',
                    'description': 'why?',
                    'type': CommandOptions.STRING,
                    'required': True
                }
            ]
        },
        {
            "name": "leaderboard",
            "description": "view the leaderbord",
            "type": CommandOptions.SUB_COMMAND,
        },
        {
            "name": "check-score",
            "description": "check the score of a user",
            "type": CommandOptions.SUB_COMMAND,
            "options": [
                {
                    'name': 'user',
                    'description': 'the user to check',
                    'type': CommandOptions.USER,
                    'required': True
                }
            ]
        }
    ]
}

POINTS_MANAGEMENT = {
    'name': 'points-manage',
    'description': 'manage the settings of the bot',
    "default_member_permissions": create_permission_number([Permission.ADMINISTRATOR]),
    'options': [
        {
            'name': 'set-points-name',
            'description': 'Set a custom prefix for the points',
            'type': CommandOptions.SUB_COMMAND,
            'options': [
                {
                    'name': 'prefix',
                    'description': 'Something that will get prefixed to "Points"',
                    'type': CommandOptions.STRING,
                    'required': True
                }
            ]
        },
        {
            'name': 'set-infinite-role',
            'description': 'Set which role gets to distribute infinite points',
            'type': CommandOptions.SUB_COMMAND,
            'options': [
                {
                    'name': 'role',
                    'description': 'The role that gets to distribute infinite points',
                    'type': CommandOptions.ROLE,
                    'required': True
                }
            ]
        },
        {
            'name': 'set-create-betting-game-role',
            'description': 'Set which role can start betting games',
            'type': CommandOptions.SUB_COMMAND,
            'options': [
                {
                    'name': 'role',
                    'description': 'The role that gets to distribute infinite points',
                    'type': CommandOptions.ROLE,
                    'required': True
                }
            ]
        },
        {
            'name': 'set-anniversary',
            'description': "Set how many points will be given on a members joined date anniversary",
            'type': CommandOptions.SUB_COMMAND,
            'options': [
                {
                    'name': 'amount',
                    'description': 'how many points will be added or subtracted',
                    'type': CommandOptions.INTEGER,
                    'required': True
                },
                {
                    'name': 'channel',
                    'description': 'Channel where the bot will poast anniversary message in',
                    'type': CommandOptions.CHANNEL,
                    'required': True
                }
            ]
        },
        {
            'name': 'delete-current-betting',
            'description': 'Delete the running betting game',
            'type': CommandOptions.SUB_COMMAND,
        },
        {
            'name': 'view-settings',
            'description': 'View the settings for this Server',
            'type': CommandOptions.SUB_COMMAND
        }
    ]
}

BETTING_COMMANDS = {
    'name': 'betting',
    'description': 'make a bet!',
    'options': [
        {
            'name': 'create-game',
            'description': 'create a betting game',
            'type': CommandOptions.SUB_COMMAND,
            'options': [
                {
                    'name': 'bet',
                    'description': 'the bet, should be a yes or no question',
                    'type': CommandOptions.STRING,
                    'required': True
                }
            ]
        },
        {
            'name': 'place-bet',
            'description': 'place a bet in the current game',
            'type': CommandOptions.SUB_COMMAND,
            'options': [
                {
                    'name': 'yes_or_no',
                    'description': 'Place bet on yes or no?',
                    'type': CommandOptions.STRING,
                    'required': True
                },
                {
                    'name': 'amount',
                    'description': 'How many points do you want to bet?',
                    'type': CommandOptions.INTEGER,
                    'required': True
                }
            ]
        },
        {
            'name': 'close-betting',
            'description': 'stop taking new bets for the current game',
            'type': CommandOptions.SUB_COMMAND,
        },
        {
            'name': 'resolve-bet',
            'description': 'resolve the current game',
            'type': CommandOptions.SUB_COMMAND,
            'options': [
                {
                    'name': 'result',
                    'description': 'the result, should be "yes" or "no"',
                    'type': CommandOptions.STRING,
                    'required': True
                }
            ]
        },
        {
            'name': 'view-bets',
            'description': 'view the bets in the current game',
            'type': CommandOptions.SUB_COMMAND,
        }
    ]
}
