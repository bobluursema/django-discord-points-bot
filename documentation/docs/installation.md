# Installation in a Django project

This is to install the bot on your own Django site. To use my deployment in your Discord server, [see the homepage.](./index.md)

1. Run `pip install django-discord-points-bot --extra-index-url https://gitlab.com/api/v4/projects/37154331/packages/pypi/simple`
2. Add 'discordbot' to `INSTALLED_APPS`
3. Add the needed settings, see the Discord Developer Portals for details:

```python
DISCORD_BOT = {
    'TOKEN': 'base64 stuff',
    'APP_ID': 'numbers',
    'CLIENT_PUBLIC_KEY': 'hex stuff'
}
```

4. Include the urls in your urls config, for example `path('discordbot', include('discordbot.urls'))`
5. Run migrations
6. Run the command to create the discord slash commands: `python manage.py register_discord_commands`
7. Create a daily cron job to run `python manage.py check_anniversary`