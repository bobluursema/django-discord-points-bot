# Contributing to the bot

## Installation

Make sure you have Python 3.9 or later installed. Then run some commands:

```
git clone https://gitlab.com/bobluursema/django-discord-points-bot.git
cd django-discord-points-bot
python -m venv .venv
pip install -r requirements.txt
python manage.py test
```

And then the tests should succeed!

## Project setup

The project is a reusable [Django](https://www.djangoproject.com/) application.

```
discordbot -> Source code of the Django application
  - discord
    - api.py -> A class and some functions to help interact with the Discord API
    - types.py -> Some classes to give helpful names to the integers from the Discord API
    - bot_commands.py -> The slash commands of the bot
  - fixtures -> Database test data
  - interactions -> Handlers that process the interactions that Discord sends
  - management -> Django management commands for the bot, one for processing anniversaries, and one to add slash commands to Discord
  - migrations -> Django directory for database migrations files
  - models.py -> Django database tables
  - views.py -> Sends a request to the correct interaction handler
documentation -> Content of the website
tests -> All tests and related files are located here
```

## Test with Discord

The best way to test is of course with an actual connection with Discord. But to do that you need to have the application available behind an HTTPS connection. In the Discord Developer Portal they suggest using [ngrok](https://ngrok.com) to connect Discord to a local development server. I haven't tried that since I already have my own server with an HTTPS connection that I can use.

I have cloned the codebase to my server and I open that folder via the Remote-SSH functionality of VS Code. Then `cd` to `tests` and run `source start_live_test.sh`. This starts a Docker container in my Docker network, mounts the codebase in it and runs the Django development server. I have setup the existing NGINX reverse proxy in the Docker network to forwards requests to this container.

The automatic reload on code changes functionality in Django keeps working with this setup so this works great for quick iterations.

To setup the testing endpoint in Discord: go to the Discord Developer portal, create an application and put the necessary information in a local_settings.py file. Then create or find a Discord server to use for testing and add the application to that server.
