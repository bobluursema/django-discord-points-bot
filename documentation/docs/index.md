# Django Discord Points Bot

This bot enables you to give out meaningless points to your members, which they can give or take from each other and they can use them to bet in games.

## Install in Discord Server

[Add to your Discord server](https://discord.com/api/oauth2/authorize?client_id=989277233195667483&permissions=2048&scope=bot%20applications.commands)

## Points

Use the `/points give :user :amount :reason` command to give or take away (by filling in a negative amount) points from someone. Everyone can use this command, but if you don't have the infinite points role (see the administration section below) this action will take away the same amount of points from your score.

Use `/points leaderboard` to view the current top ten point hoarders.

Hit `/points check-score :user` to view a users score, which users are close to them in score and how many points have passed by them.

## Betting

With `/betting create-game :bet` everyone can start a bet, the `bet` should be a yes or no question. Users can use the buttons on the message to set a bet. Every user can only set one bet. And after betting it cannot be edited. The initiator of the bet cannot bet. Only a single betting game can be active at one time.
p
A user can also place a bet with the `/betting place-bet :yes_or_no :amount` command.

Any user can view the bets in the current game with the `/betting view-bets` command.

The initiator can close the betting with `/betting close-betting`, then no new bets will be allowed in the game.

The initiator of the bet can resolve the bet with the `/betting resolve-bet :result` command, filling in whether yes or no has won. The winners gain the amount that they bet, the losers lose the amount that they bet.

## Administration

Administrators have access to a couple more commands.

`/points-manage set-points-name :prefix`, with this command you can set a custom prefix for the points. For example you can use the prefix "Bonus" so that everywhere the points will be named "BonusPoints".

`/points-manage set-infinite-role :role`, users with this role can give or take away points without it impacting their own point total.

`/points-manage set-anniversary :amount :channel`, if you use this command the bot will automatically give users points every year on the anniversary of their join date on the server. Assuming that they have at least once received points so the bot knows of their existence.

`/points-manage set-create-betting-game-role :role`, only users with this role can start a betting game. If not set everyone can start a game.

`/points-manage delete-current-betting`, this deletes any current running betting game. Can be used if the initiator of the game can't or won't resolve the running game.

`/points-manage view-settings`, this sends a message with the current settings for the server.
